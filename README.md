# Docker image with rce-agent for Node.js

An OCI-compliant image to test
[github.com/square/rce-agent](https://github.com/square/rce-agent) capabilities
in containerized environments.

[![pipeline
status](https://gitlab.com/ricardomendes/docker-rce-agent-nodejs/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/docker-rce-agent-nodejs/-/commits/master)
