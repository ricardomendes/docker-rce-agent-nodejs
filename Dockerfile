# ============================================================================ #
# The client and server are built in the first stage.                          #
# ============================================================================ #
FROM golang:1.14 AS build

WORKDIR /build

RUN git clone https://github.com/square/rce-agent.git \
    && cd ./rce-agent/example/client \
    && go build \
    && cd ../server \
    && go build

# ============================================================================ #
# Build the lightweight resulting image.                                       #
# ============================================================================ #
FROM debian:buster-slim

WORKDIR /rce-agent-nodejs

COPY --from=build /build/rce-agent/example/client/client .

COPY --from=build /build/rce-agent/example/server/server .
COPY agent-entrypoint.sh .
COPY commands.yaml .

RUN chmod +x agent-entrypoint.sh

# ---------------------------------------------------------------------------- #
# Install and configure sshd.                                                  #
# https://docs.docker.com/engine/examples/running_ssh_service for reference.   #
# ---------------------------------------------------------------------------- #
RUN apt-get update -qq -y \
    && apt-get install -qq -y openssh-server \
    # Creating /run/sshd instead of /var/run/sshd, because in the Debian
    # image /var/run is a symlink to /run. Creating /var/run/sshd directory
    # as proposed in the Docker documentation linked above just doesn't
    # work.
    && mkdir -p /run/sshd

EXPOSE 22

# ---------------------------------------------------------------------------- #
# Install GitLab CI required dependencies.                                     #
# ---------------------------------------------------------------------------- #                 
ARG GITLAB_RUNNER_VERSION=v12.9.0

RUN apt-get install -qq -y curl \
    && curl -Lo /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/${GITLAB_RUNNER_VERSION}/binaries/gitlab-runner-linux-amd64 \
    && chmod +x /usr/local/bin/gitlab-runner

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -qq -y git-lfs \
    && git lfs install --skip-repo

# ---------------------------------------------------------------------------- #
# Install https://github.com/krallin/tini - a very small 'init' process        #
# that helps processing signalls sent to the container properly.               #
# ---------------------------------------------------------------------------- #
ARG TINI_VERSION=v0.19.0

RUN curl -Lo /usr/local/bin/tini https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-amd64 \
    && chmod +x /usr/local/bin/tini

# ---------------------------------------------------------------------------- #
# Execute a startup script.                                                    #
# https://success.docker.com/article/use-a-script-to-initialize-stateful-container-data
# for reference.                                                               #
# ---------------------------------------------------------------------------- #
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["tini", "--", "/usr/local/bin/docker-entrypoint.sh"]
